import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    selectedRange: any = "No range selected";
    constructor( 
        public changeDetector: ChangeDetectorRef
    ) {
    }
      
    ngOnInit() {
        this.addSelectionChangeListner();
    }

    async addSelectionChangeListner() {
        await Excel.run( { delayForCellEdit: true }, async context => {
            const sheet = context.workbook.worksheets.getActiveWorksheet(); // v1.1
            sheet.onSelectionChanged.add(this.onSelectionChange);
            return context.sync();
        });
    }

    onSelectionChange = async (event: Excel.WorksheetSelectionChangedEventArgs) => {
        this.selectedRange = event.address;
        // Angular should do the changes without below ( line 31 ) line I added this because after the listner is 
        // triggered somthing stops angular from detecting changes automatically
        this.changeDetector.detectChanges();
    }
}